#!/usr/bin/env bash
run_successful=true

for dir in */
do
  echo "Testing '${dir%*/}'..."
  cd ${dir}
  dotnet test
  if [ $? -eq 0 ]
  then
    echo "...'${dir%*/}' all ok!"
  else
    echo "...'${dir%*/}' NOT all ok!"
    run_successful=false
  fi
  cd ..
done

if [ "$run_successful" = true ]
then
  echo "All tests were completed successfully!"
  exit 0
else
  exit 1
fi
